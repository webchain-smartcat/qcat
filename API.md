# Webchain API documentation

## Version: 1.7.0

**License:** [MIT](https://gitlab.com/webchain-smartcat/qcat/-/raw/main/LICENSE)

## PRE-REQUISITE

Register your app with SmartCat, Inc. Please contact [info@smartcat.tech](mailto:info@smartcat.tech)

## URL Endpoint

### https://webchain-api.com/v1/



### /webchain/auth/register

#### POST

##### Summary:

Register as user

##### Responses

| Code | Description |
| ---- | ----------- |
| 201  | Created     |
| 400  |             |

### /webchain/auth/login

#### POST

##### Summary:

Login

##### Responses

| Code | Description               |
| ---- | ------------------------- |
| 200  | OK                        |
| 401  | Invalid email or password |

### /webchain/auth/refresh-tokens

#### POST

##### Summary:

Refresh auth tokens

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |

### /webchain/tx/

#### GET

##### Summary:

Get all transactions

##### Description:

Only authorized apps can retrieve all transactions.

##### Parameters

| Name   | Located in | Description                                     | Required | Schema  |
| ------ | ---------- | ----------------------------------------------- | -------- | ------- |
| sortBy | query      | sort by query in the form of desc/asc (ex:desc) | No       | string  |
| limit  | query      | Maximum number of transfers                     | No       | integer |
| page   | query      | Page number                                     | No       | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |
| 403  |             |

##### Security

| Security Schema | Scopes |
| --------------- | ------ |
| bearerAuth      |        |

### /webchain/tx/{hash}

#### GET

##### Summary:

Get a transaction by hash

##### Description:

Must be authenticated.

##### Parameters

| Name | Located in | Description      | Required | Schema |
| ---- | ---------- | ---------------- | -------- | ------ |
| hash | path       | Transaction hash | Yes      | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |
| 403  |             |
| 404  |             |

##### Security

| Security Schema | Scopes |
| --------------- | ------ |
| bearerAuth      |        |

### /webchain/wallet/tx/{address}

#### GET

##### Summary:

Get all transactions

##### Description:

Only authorized apps can retrieve all transactions.

##### Parameters

| Name   | Located in | Description                                     | Required | Schema  |
| ------ | ---------- | ----------------------------------------------- | -------- | ------- |
| sortBy | query      | sort by query in the form of desc/asc (ex:desc) | No       | string  |
| limit  | query      | Maximum number of transfers                     | No       | integer |
| page   | query      | Page number                                     | No       | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |
| 403  |             |

##### Security

| Security Schema | Scopes |
| --------------- | ------ |
| bearerAuth      |        |

### /webchain/wallet/qcat/{address}

#### GET

##### Summary:

Get QCAT wallet balance of user

##### Description:

Only authenticated user can access his own wallet's balance

##### Parameters

| Name    | Located in | Description    | Required | Schema |
| ------- | ---------- | -------------- | -------- | ------ |
| address | path       | Wallet address | Yes      | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |
| 403  |             |
| 404  |             |

##### Security

| Security Schema | Scopes |
| --------------- | ------ |
| bearerAuth      |        |

### /webchain/wallet/sea/{address}

#### GET

##### Summary:

Get SEA wallet balance of user

##### Description:

Only authenticated user can access his own wallet's balance

##### Parameters

| Name    | Located in | Description    | Required | Schema |
| ------- | ---------- | -------------- | -------- | ------ |
| address | path       | Wallet address | Yes      | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200  | OK          |
| 401  |             |
| 403  |             |
| 404  |             |

##### Security

| Security Schema | Scopes |
| --------------- | ------ |
| bearerAuth      |        |


### /webchain/wallet/sendToken

#### POST
##### Summary:

Send tokens from a wallet

##### Description:

Only authenticated user can access perform this task

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | OK |
| 401 |  |
| 403 |  |
| 404 |  |

##### Security

| Security Schema | Scopes |
| --- | --- |
| bearerAuth | |
